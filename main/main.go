package main

import (
	"fmt"
	"unsafe"
)

func main() {
	var c chan int
	var m map[int]int
	var f func()
	var s []int
	var str string
	var a any
	fmt.Println(unsafe.Sizeof(c))
	fmt.Println(unsafe.Sizeof(m))
	fmt.Println(unsafe.Sizeof(f))
	fmt.Println(unsafe.Sizeof(s))
	fmt.Println(unsafe.Sizeof(str))
	fmt.Println(unsafe.Sizeof(a))
}
