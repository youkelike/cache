package lru

// 实现控制 key 数量的 LRU 缓存，无锁、无过期时间
// 添加头尾节点指针，并且在初始化的时候把它们连接起来，可以大大简化判断，代码更简洁
// 因为访问时涉及到挪动操作，要在 o(1) 的时间复杂度实现，只能用链表
// map 用于快速定位节点
type LRUCache struct {
	m        map[string]*Node
	capacity int
	count    int
	// 头尾指针不存数据，单向链接到头尾节点
	head *Node
	tail *Node
}

type Node struct {
	key  string
	val  any
	prev *Node
	next *Node
}

func Constructor(capacity int) *LRUCache {
	l := LRUCache{
		m:        make(map[string]*Node, capacity),
		capacity: capacity,
		head:     &Node{},
		tail:     &Node{},
	}
	l.head.next = l.tail
	l.tail.prev = l.head
	return &l
}

func (c *LRUCache) Get(key string) any {
	n, ok := c.m[key]
	if !ok {
		return -1
	}
	c.RemoveNode(n)
	c.AddToHead(n)
	return n.val
}

func (c *LRUCache) Put(key string, value any) {
	n, ok := c.m[key]
	if ok {
		n.val = value
		c.RemoveNode(n)
		c.AddToHead(n)
		return
	}

	if c.capacity > 0 && c.count == c.capacity {
		tail := c.RemoveTail()
		delete(c.m, tail.key)
		c.count--
	}
	c.count++
	n = &Node{key: key, val: value}
	c.AddToHead(n)
	c.m[key] = n
}

func (c *LRUCache) Delete(key string) {
	n, ok := c.m[key]
	if !ok {
		return
	}
	c.RemoveNode(n)
	delete(c.m, key)
}

func (c *LRUCache) Pop() string {
	n := c.RemoveTail()
	delete(c.m, n.key)
	return n.key
}

func (c *LRUCache) RemoveNode(n *Node) {
	n.prev.next = n.next
	n.next.prev = n.prev
}

func (c *LRUCache) RemoveTail() *Node {
	tail := c.tail.prev
	c.RemoveNode(tail)
	return tail
}

func (c *LRUCache) AddToHead(n *Node) {
	// 改变原先头结点的前驱节点为新的节点
	n.next = c.head.next
	c.head.next.prev = n
	// 改变新头结点的前驱节点为 head 指针
	c.head.next = n
	n.prev = c.head
}
