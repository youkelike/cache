package cache

import (
	"context"
	"time"

	"golang.org/x/sync/singleflight"
)

type SingleFlightCache struct {
	ReadThroughCache
}

func NewSingleFlightCache(cache Cache,
	loadFunc func(ctx context.Context, key string) (any, error),
	expiration time.Duration) *SingleFlightCache {
	g := &singleflight.Group{}
	return &SingleFlightCache{
		ReadThroughCache: ReadThroughCache{
			Cache: cache,
			LoadFunc: func(ctx context.Context, key string) (any, error) {
				val, err, _ := g.Do(key, func() (any, error) {
					return loadFunc(ctx, key)
				})
				return val, err
			},
			Expiration: expiration,
		},
	}
}
