package cache

import (
	"reflect"
	"unsafe"
)

func SizeOf(v interface{}) int {
	// Cache 保存访问过的指针对象，防止重复计算同一个指针的大小
	cache := make(map[unsafe.Pointer]bool)
	return sizeOf(reflect.Indirect(reflect.ValueOf(v)), cache)
}

func sizeOf(v reflect.Value, cache map[unsafe.Pointer]bool) int {
	switch v.Kind() {

	case reflect.Array:
		// 数组递归计算每个成员的大小，最后加总
		sum := 0
		for i := 0; i < v.Len(); i++ {
			s := sizeOf(v.Index(i), cache)
			if s < 0 {
				return -1
			}
			sum += s
		}

		// return sum + (v.Cap()-v.Len())*int(v.Type().Elem().Size())
		return sum

	case reflect.Slice:
		if v.IsNil() {
			return int(v.Type().Size())
		}
		// 切片递归计算每个成员的大小，还要加上切片底层数组上未分配元素的空间大小、切片底层三元组结构的大小
		if cache[v.UnsafePointer()] {
			return 0
		}
		cache[v.UnsafePointer()] = true

		sum := 0
		for i := 0; i < v.Len(); i++ {
			s := sizeOf(v.Index(i), cache)
			if s < 0 {
				return -1
			}
			sum += s
		}

		sum += (v.Cap() - v.Len()) * int(v.Type().Elem().Size())
		return sum + int(v.Type().Size())

	case reflect.Struct:
		// 结构体递归计算每个成员的大小，还要加上结构体内存对齐的 padding
		sum := 0
		for i, n := 0, v.NumField(); i < n; i++ {
			s := sizeOf(v.Field(i), cache)
			if s < 0 {
				return -1
			}
			sum += s
		}

		// 计算结构体在内存对齐的时候，给字段之间添加的 padding 长度.
		// 结构体的总体内存分配 - 每个字段类型的内存分配 = padding 长度
		padding := int(v.Type().Size())
		for i, n := 0, v.NumField(); i < n; i++ {
			padding -= int(v.Field(i).Type().Size())
		}
		return sum + padding
	case reflect.String:
		// 字符串计算长度和字符串底层二元组结构的大小
		s := v.String()
		// hdr := (*reflect.StringHeader)(unsafe.Pointer(&s))
		// if cache[hdr.Data] {
		// 	return int(v.Type().Size())
		// }
		// cache[hdr.Data] = true
		return len(s) + int(v.Type().Size())

	case reflect.Pointer:
		if v.IsNil() {
			return int(v.Type().Size())
		}

		// 如果是空指针，计算指针占用就行了；
		// 不是空指针就还要加上指针指向数据的大小
		if cache[v.UnsafePointer()] {
			return int(v.Type().Size())
		}
		cache[v.UnsafePointer()] = true
		if v.IsNil() {
			return int(v.Type().Size())
			// return int(reflect.New(v.Type()).Type().Size())
		}
		s := sizeOf(reflect.Indirect(v), cache)
		if s < 0 {
			return -1
		}
		return s + int(v.Type().Size())

	case reflect.Bool,
		reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64,
		reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64,
		reflect.Int, reflect.Uint,
		reflect.Chan,
		reflect.Uintptr,
		reflect.Float32, reflect.Float64, reflect.Complex64, reflect.Complex128,
		reflect.Func:
		return int(v.Type().Size())

	case reflect.Map:
		if v.IsNil() {
			return int(v.Type().Size())
		}

		// 字典要递归计算每个 key 大小、value 大小、字典本身类型大小、和未使用空间大小
		if cache[v.UnsafePointer()] {
			return 0
		}
		cache[v.UnsafePointer()] = true
		sum := 0
		keys := v.MapKeys()
		for i := range keys {
			val := v.MapIndex(keys[i])
			// calculate size of key and value separately
			sv := sizeOf(val, cache)
			if sv < 0 {
				return -1
			}
			sum += sv
			sk := sizeOf(keys[i], cache)
			if sk < 0 {
				return -1
			}
			sum += sk
		}
		// Include overhead due to unused map buckets.  10.79 comes
		// from https://golang.org/src/runtime/map.go.
		return sum + int(v.Type().Size()) + int(float64(len(keys))*10.79)

	case reflect.Interface:
		if v.IsNil() {
			return int(v.Type().Size())
		}

		// 递归接口计算接口中包含的变量大小、接口类型本身的大小
		return sizeOf(v.Elem(), cache) + int(v.Type().Size())
	}

	return -1
}
