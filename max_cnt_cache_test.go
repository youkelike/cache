package cache

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestMaxCntLRUCache_Get(t *testing.T) {
	testCases := []struct {
		name    string
		key     string
		cache   func() *MaxCntLRUCache
		wantVal any
		wantErr error
	}{
		{
			name: "key not found",
			key:  "not exist key",
			cache: func() *MaxCntLRUCache {
				return NewMaxCntLRUCache(2)
			},
			wantErr: fmt.Errorf("%w, key: %s", errKeyNotFound, "not exist key"),
		},
		{
			name: "get value",
			key:  "key1",
			cache: func() *MaxCntLRUCache {
				res := NewMaxCntLRUCache(2)
				err := res.Set(context.Background(), "key1", "val1", time.Second*10)
				require.NoError(t, err)
				return res
			},
			wantVal: "val1",
		},
		{
			name: "expire key",
			key:  "key1",
			cache: func() *MaxCntLRUCache {
				res := NewMaxCntLRUCache(2)
				err := res.Set(context.Background(), "key1", "val1", time.Second)
				require.NoError(t, err)
				time.Sleep(time.Second * 2)
				return res
			},
			wantErr: fmt.Errorf("%w, key: %s", errKeyNotFound, "key1"),
		},
		{
			name: "容量为 1 时，key1 因为容量超标被挤出去",
			key:  "key1",
			cache: func() *MaxCntLRUCache {
				res := NewMaxCntLRUCache(1)
				err := res.Set(context.Background(), "key1", "val1", time.Second*10)
				require.NoError(t, err)
				err = res.Set(context.Background(), "key2", "val2", time.Second*10)
				require.NoError(t, err)
				return res
			},
			wantErr: fmt.Errorf("%w, key: %s", errKeyNotFound, "key1"),
		},
		{
			name: "容量为 2 时，key1 因为容量超标被挤出",
			key:  "key1",
			cache: func() *MaxCntLRUCache {
				res := NewMaxCntLRUCache(2)
				err := res.Set(context.Background(), "key1", "val1", time.Second*10)
				require.NoError(t, err)
				err = res.Set(context.Background(), "key2", "val2", time.Second*10)
				require.NoError(t, err)
				err = res.Set(context.Background(), "key3", "val3", time.Second*10)
				require.NoError(t, err)
				return res
			},
			wantErr: fmt.Errorf("%w, key: %s", errKeyNotFound, "key1"),
		},
		{
			name: "容量为 2 时，key1 没有因为容量超标被挤出",
			key:  "key1",
			cache: func() *MaxCntLRUCache {
				res := NewMaxCntLRUCache(2)
				err := res.Set(context.Background(), "key1", "val1", time.Second*10)
				require.NoError(t, err)
				err = res.Set(context.Background(), "key2", "val2", time.Second*10)
				require.NoError(t, err)
				res.Get(context.Background(), "key1")
				err = res.Set(context.Background(), "key3", "val3", time.Second*10)
				require.NoError(t, err)
				return res
			},
			wantVal: "val1",
		},
		{
			name: "容量超标时可以插入新的键值对",
			key:  "key2",
			cache: func() *MaxCntLRUCache {
				res := NewMaxCntLRUCache(1)
				err := res.Set(context.Background(), "key1", "val1", time.Second*10)
				require.NoError(t, err)
				err = res.Set(context.Background(), "key2", "val2", time.Second*10)
				require.NoError(t, err)
				return res
			},
			wantVal: "val2",
		},
		{
			name: "repeat key",
			key:  "key1",
			cache: func() *MaxCntLRUCache {
				res := NewMaxCntLRUCache(1)
				err := res.Set(context.Background(), "key1", "val1", time.Second*1)
				require.NoError(t, err)
				err = res.Set(context.Background(), "key1", "val2", time.Second*10)
				require.NoError(t, err)
				return res
			},
			wantVal: "val2",
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			c := tc.cache()
			val, err := c.Get(context.Background(), tc.key)
			assert.Equal(t, tc.wantErr, err)
			if err != nil {
				return
			}
			assert.Equal(t, tc.wantVal, val)
			c.Close()
		})
	}
}
