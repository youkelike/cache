package cache

import (
	"context"
	"log"
	"time"

	"golang.org/x/sync/singleflight"
)

// 只专注于处理业务方的读请求，
// 业务方在缓存中没读到数据时，框架负责后续的读 DB、更新缓存
// 业务方需要提供从 DB 读数据的方法
type ReadThroughCache struct {
	Cache
	LoadFunc   func(ctx context.Context, key string) (any, error)
	Expiration time.Duration
	g          singleflight.Group
}

// 同步写法
func (r *ReadThroughCache) Get(ctx context.Context, key string) (any, error) {
	val, err := r.Cache.Get(ctx, key)
	if err == errKeyNotFound {
		// 用户自定义的，去 DB 取数据的方法
		val, err := r.LoadFunc(ctx, key)
		// 从 DB 取回数据后写回缓存
		if err == nil {
			err = r.Cache.Set(ctx, key, val, r.Expiration)
			if err != nil {
				log.Fatalf("cache: 刷新缓存失败 %v", err)
			}
		}
	}
	return val, err
}

// singleflight 写法
func (r *ReadThroughCache) GetV2(ctx context.Context, key string) (any, error) {
	val, err := r.Cache.Get(ctx, key)
	if err == errKeyNotFound {
		val, err, _ = r.g.Do(key, func() (any, error) {
			// 用户自定义的，去 DB 取数据的方法
			v, er := r.LoadFunc(ctx, key)
			// 从 DB 取回数据后写回缓存
			if er == nil {
				er = r.Cache.Set(ctx, key, val, r.Expiration)
				if er != nil {
					log.Fatalf("cache: 刷新缓存失败 %v", err)
				}
			}
			return v, er
		})
	}
	return val, err
}
