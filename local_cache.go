package cache

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"time"
)

var (
	errKeyNotFound = errors.New("cache: 键不存在")
)

// 实现基本的缓存增删读功能，和 key 过期清理
type BuildInMapCache struct {
	data      map[string]item
	mutex     sync.RWMutex
	close     chan struct{}
	onEvicted func(key string, val any)
}

type BuildInMapCacheOption func(*BuildInMapCache)

func WithEvictedCallback(fn func(key string, val any)) BuildInMapCacheOption {
	return func(b *BuildInMapCache) {
		b.onEvicted = fn
	}
}

// 实例化的时候就要开启垃圾检查
func NewBuildInMapCache(interval time.Duration, opts ...BuildInMapCacheOption) *BuildInMapCache {
	res := &BuildInMapCache{
		data:      make(map[string]item, 100),
		close:     make(chan struct{}),
		onEvicted: func(key string, val any) {},
	}
	for _, op := range opts {
		op(res)
	}

	// 定时垃圾回收，还要考虑退出机制
	// 对过期数据的检查还可以在 get 的时候进行
	go func() {
		ticker := time.NewTicker(interval)
		for {
			select {
			case t := <-ticker.C:
				res.mutex.Lock()
				i := 0
				for key, val := range res.data {
					if i > 1000 {
						break
					}
					if val.deadlineBefore(t) {
						res.delete(key)
						// delete(res.data, key)
					}
					i++
				}
				res.mutex.Unlock()
			case <-res.close:
				return
			}
		}
	}()
	return res
}

// 存要考虑的点：加锁、过期时间
func (b *BuildInMapCache) Set(ctx context.Context, key string, val any, expiration time.Duration) error {
	b.mutex.Lock()
	defer b.mutex.Unlock()
	return b.set(key, val, expiration)
}

// 拆分出来一个不含锁的方法，是为了方便其它地方的加锁调用
func (b *BuildInMapCache) set(key string, val any, expiration time.Duration) error {
	var dl time.Time
	if expiration > 0 {
		dl = time.Now().Add(expiration)
	}
	b.data[key] = item{
		val:      val,
		deadline: dl,
	}
	return nil
}

// 取要考虑的点：读写锁、检查过期、过期删除
func (b *BuildInMapCache) Get(ctx context.Context, key string) (any, error) {
	b.mutex.RLock()
	val, ok := b.data[key]
	b.mutex.RUnlock()
	if !ok {
		return nil, fmt.Errorf("%w, key: %s", errKeyNotFound, key)
	}
	// 返回前执行过期检查
	now := time.Now()
	if !val.deadlineBefore(now) {
		return val.val, nil
	}

	b.mutex.Lock()
	defer b.mutex.Unlock()
	val, ok = b.data[key]
	if !ok {
		return nil, fmt.Errorf("%w, key: %s", errKeyNotFound, key)
	}
	now = time.Now()
	if !val.deadlineBefore(now) {
		return val.val, nil
	}

	b.delete(key)
	return nil, fmt.Errorf("%w, key: %s", errKeyNotFound, key)
}

// 删除要考虑：写锁、是否存在、CDC
func (b *BuildInMapCache) Delete(ctx context.Context, key string) error {
	b.mutex.Lock()
	defer b.mutex.Unlock()
	b.delete(key)
	return nil
}

// 拆分出来一个不含锁的方法，是为了方便其它地方的加锁调用
func (b *BuildInMapCache) delete(key string) {
	itm, ok := b.data[key]
	if !ok {
		return
	}
	delete(b.data, key)
	b.onEvicted(key, itm.val)
}

func (b *BuildInMapCache) LoadAndDelete(ctx context.Context, key string) (any, error) {
	b.mutex.Lock()
	defer b.mutex.Unlock()

	val, ok := b.data[key]
	if !ok {
		return nil, fmt.Errorf("%w, key: %s", errKeyNotFound, key)
	}
	delete(b.data, key)
	b.onEvicted(key, val.val)
	return val, nil
}

func (b *BuildInMapCache) Close() error {
	close(b.close)
	return nil
}

type item struct {
	val      any
	deadline time.Time
}

func (i *item) deadlineBefore(t time.Time) bool {
	return !i.deadline.IsZero() && i.deadline.Before(t)
}
