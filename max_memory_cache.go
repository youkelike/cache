package cache

import (
	"context"
	"fmt"
	"sync"
	"time"
)

var _ Cache = &MaxSizeLRUCache{}

// 有锁、有过期时间控制、有键值对数量限制的，符合 LRU 规则的缓存
type MaxSizeLRUCache struct {
	// 链表头尾节点不存数据，只为方便操作，它们是两个不同的空节点
	head, tail *Data
	// 节点索引，方便快速通过 key 找到节点
	m         map[string]*Data
	maxSize   int
	used      int
	close     chan struct{}
	mu        sync.Mutex
	onEvicted func(key string, val any)
}

func NewMaxSizeLRUCache(maxSize int) *MaxSizeLRUCache {
	c := &MaxSizeLRUCache{
		maxSize: maxSize,
		m:       make(map[string]*Data, maxSize),
		head:    new(Data),
		tail:    new(Data),
		close:   make(chan struct{}),
	}
	// 头尾节点连起来
	c.head.nex = c.tail
	c.tail.prev = c.head
	// 定时、定量扫描清理过期 key
	go func() {
		timer := time.NewTicker(time.Second)
		for {
			select {
			case t := <-timer.C:
				c.mu.Lock()
				i := 0
				for key, data := range c.m {
					if data.deadlineBefore(t) {
						c.delete(key)
					}
					i++
					if i >= 1000 {
						break
					}
				}
				c.mu.Unlock()
			case <-c.close:
				return
			}
		}
	}()
	return c
}

func (c *MaxSizeLRUCache) Get(ctx context.Context, key string) (any, error) {
	c.mu.Lock()
	defer c.mu.Unlock()

	data, ok := c.m[key]
	// 不存在
	if !ok {
		return nil, fmt.Errorf("%w, key: %s", errKeyNotFound, key)
	}
	// 已过期
	if data.deadlineBefore(time.Now()) {
		c.delete(key)
		return nil, fmt.Errorf("%w, key: %s", errKeyNotFound, key)
	}
	// 节点移动到链表头部
	c.RemoveNode(data)
	c.AddToHead(data)
	return data.val, nil
}

func (c *MaxSizeLRUCache) Set(ctx context.Context, key string, val any, expiration time.Duration) error {
	c.mu.Lock()
	defer c.mu.Unlock()
	return c.set(key, val, expiration)
}
func (c *MaxSizeLRUCache) set(key string, val any, expiration time.Duration) error {
	data, ok := c.m[key]
	if ok {
		// key 已存在，更新节点值、过期时间
		data.val = val
		data.expire = time.Now().Add(expiration)
		// 节点移动到头部
		c.RemoveNode(data)
		c.AddToHead(data)
		return nil
	}

	// 把超过容量的尾节点移除
	for c.used >= c.maxSize {
		c.delete(c.tail.prev.key)
	}

	data = &Data{
		key:    key,
		val:    val,
		expire: time.Now().Add(expiration),
	}
	// 加到头部
	c.AddToHead(data)
	// 建立索引
	c.m[key] = data
	// 增加节点计数
	c.used += SizeOf(val)
	return nil
}

func (c *MaxSizeLRUCache) Delete(ctx context.Context, key string) error {
	c.mu.Lock()
	defer c.mu.Unlock()
	return c.delete(key)
}
func (c *MaxSizeLRUCache) delete(key string) error {
	data, ok := c.m[key]
	if !ok {
		return errKeyNotFound
	}
	// 因为不需要返回被删除的值，所以不关心是否过期，直接删掉就行
	// 减少节点计数
	c.used -= SizeOf(data.val)
	// 移除节点
	c.RemoveNode(data)
	// 移除索引
	delete(c.m, key)
	// 删除回调
	if c.onEvicted != nil {
		c.onEvicted(key, data)
	}
	return nil
}

func (c *MaxSizeLRUCache) LoadAndDelete(ctx context.Context, key string) (any, error) {
	c.mu.Lock()
	defer c.mu.Unlock()
	data, ok := c.m[key]
	if !ok {
		return nil, errKeyNotFound
	}
	c.delete(key)
	// 因为需要返回被删除的值，所以要判断是否过期
	if data.deadlineBefore(time.Now()) {
		return nil, fmt.Errorf("%w, key: %s", errKeyNotFound, key)
	}
	return data.val, nil
}

func (c *MaxSizeLRUCache) Close() {
	close(c.close)
}

// 移除指定节点，只需把节点的前后节点连接起来
func (c *MaxSizeLRUCache) RemoveNode(data *Data) {
	data.prev.nex = data.nex
	data.nex.prev = data.prev
	data.prev = nil
	data.nex = nil
}

// 移除尾节点，只需把尾指针的前驱节点移除
func (c *MaxSizeLRUCache) RemoveTail() *Data {
	tail := c.tail.prev
	c.RemoveNode(tail)
	return tail
}

// 添加节点到头部
func (c *MaxSizeLRUCache) AddToHead(data *Data) {
	c.head.nex.prev = data
	data.nex = c.head.nex

	c.head.nex = data
	data.prev = c.head
}
