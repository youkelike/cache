package cache

import (
	"context"
	"strings"
	"testing"
)

func TestReadThroughCache_Get(t *testing.T) {
	c := ReadThroughCache{
		LoadFunc: func(ctx context.Context, key string) (any, error) {
			if strings.HasPrefix(key, "usr_") {
				// 加载 user
				panic("")
			} else if strings.HasPrefix(key, "order_") {
				// 加载 order
				panic("")
			} else {
				// 不知道怎么加载
				panic("")
			}
		},
	}

	val1, _ := c.Get(context.Background(), "user_1")
	u := val1.(User)
	t.Log(u)
	val2, _ := c.Get(context.Background(), "order_1")
	o := val2.(Order)
	t.Log(o)
}

type User struct{}
type Order struct{}
