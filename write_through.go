package cache

import (
	"context"
	"time"
)

// 只专注于处理业务方的写请求
// 接收业务方的写请求，框架负责后续的写 DB、写 cache
// 业务方需要提供将数据写入 DB 的方法
type WriteThroughCache struct {
	Cache
	StoreFunc func(ctx context.Context, key string, val any) error
}

func (c *WriteThroughCache) Set(ctx context.Context, key string, val any, expiration time.Duration) error {
	err := c.StoreFunc(ctx, key, val)
	if err != nil {
		return err
	}
	return c.Cache.Set(ctx, key, val, expiration)
}
