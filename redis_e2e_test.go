//go:buld e2e
package cache

import (
	"context"
	"testing"
	"time"

	redis "github.com/redis/go-redis/v9"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestRedisCache_e2e_Set(t *testing.T) {
	rdb := redis.NewClient(&redis.Options{
		Addr: "localhost:6379",
	})
	// rdb.Ping(context.Background())
	c := NewRedisCache(rdb)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	err := c.Set(ctx, "key1", "value1", time.Minute)
	require.NoError(t, err)
	val, err := c.Get(ctx, "key1")
	require.NoError(t, err)
	assert.Equal(t, val, "value1")
}
